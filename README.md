# GAN-Scape

GAN-Scape is a Deep Convolutional Generative Adversarial Network (DCGAN) trained to generate new instances of impressionist landscape paintings. By training a convolutional generator network to generate new paintings alongside a discriminator model which separates real paintings from fake (generated) paintings, GAN-Scape simulates a zero-sum game with the generator in competition with the discriminator. As the generator learns to produce more realistic paintings, the discriminator learns to better discriminate between real and fake. In the end, we are left with two models; one capable of generating convincing (or at least interesting) images, the other capable of effectively discriminating real paintings from generated ones. Because of the beautiful art I have seen DCGANs generate, I found this to be a very interesting project to explore.

Below are some examples of real and generated impressionist paintings.

## Real Impressionist Paintings
<img src="/real_grid.png"  width="550" height="550">


## Generated Impressionist Paintings
<img src="/gan_grid.png"  width="550" height="550">





